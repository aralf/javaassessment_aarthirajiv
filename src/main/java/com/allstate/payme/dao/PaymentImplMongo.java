package com.allstate.payme.dao;

import com.allstate.payme.entities.Payment;
import com.allstate.payme.exceptions.*;
import com.mongodb.*;
import com.mongodb.client.*;
import com.mongodb.client.MongoClient;
import com.mongodb.client.result.InsertOneResult;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import static org.bson.codecs.configuration.CodecRegistries.*;
import static com.mongodb.client.model.Filters.*;
import java.util.*;

public class PaymentImplMongo implements PaymentRepo {

    private MongoCollection<Payment> payments;

    public PaymentImplMongo() {
        ConnectionString connectionString = new ConnectionString("mongodb://localhost:27017");
        CodecRegistry pojoCodecRegistry = fromProviders(PojoCodecProvider.builder().automatic(true).build());
        CodecRegistry codecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), pojoCodecRegistry);
        MongoClientSettings clientSettings = MongoClientSettings.builder()
                .applyConnectionString(connectionString).codecRegistry(codecRegistry).build();

        MongoClient mongoClient = MongoClients.create(clientSettings);
        MongoDatabase db = mongoClient.getDatabase("assessment");
        payments = db.getCollection("payments",  Payment.class);
    }

    @Override
    public int rowCount() {
        //get row count
        long count = 0;
        try {
            count = payments.countDocuments();
        } catch(Exception e) {
            e.printStackTrace();
        }
        System.out.println("**** Number of Payments found: " + count);
        return (int)count;
    }

    @Override
    public Payment findById(int id) {
        //get by Id
        Payment payment = null;
        try {
            payment = payments.find(eq("_id", id)).first();
        } catch(MongoException e) {
            e.printStackTrace();
        }
        return payment;
    }

    @Override
    public List<Payment> findByType(String type) {
        //get by type
        List<Payment> paymentList = new ArrayList<Payment>();
        FindIterable<Payment> allDbPayments = this.payments.find(eq("type", type));
        Iterator<Payment> paymentIterator = allDbPayments.iterator();

        while(paymentIterator.hasNext()) {
            paymentList.add(paymentIterator.next());
        }
        return paymentList;
    }

    @Override
    public int save(Payment payment) {
        // insert
        int savedId = savedId = payments.insertOne(payment).getInsertedId().asInt32().intValue();
        return savedId;
    }

    @Override
    public boolean isConnected() {
        //implement later
        return true;
    }
}
