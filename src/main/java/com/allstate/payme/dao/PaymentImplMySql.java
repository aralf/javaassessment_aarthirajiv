package com.allstate.payme.dao;

import com.allstate.payme.entities.Payment;

import java.sql.*;
import java.util.*;

public class PaymentImplMySql implements PaymentRepo {

    private Connection connection;

    public PaymentImplMySql() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            this.connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/payments?serverTimezone=UTC&useSSL=false",
                    "root", "c0nygre");
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }
    @Override
    public boolean isConnected() {
        Statement statement = null;
        try {
            statement = this.connection.createStatement();
            ResultSet rs=statement.executeQuery("select 1");
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }

    @Override
    public int rowCount() {
        int count = 0;
        try {
            PreparedStatement preparedStatement= this.connection.
                    prepareStatement("Select COUNT(*) From Payments");

            ResultSet resultSet= preparedStatement.executeQuery();
            resultSet.first();
            count = resultSet.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            return count;
        }
    }

    @Override
    public Payment findById(int id) {
        try {
            PreparedStatement preparedStatement= this.connection.
                    prepareStatement("Select * From Payments where id=?");

            preparedStatement.setInt(1, id);
            ResultSet resultSet= preparedStatement.executeQuery();
            resultSet.first();
            Payment payment = new Payment(
                    resultSet.getInt("id"),
                    resultSet.getDate("paymentDate"),
                    resultSet.getString("paymentType"),
                    resultSet.getInt("amount"),
                    resultSet.getInt("custId")
            );
            return  payment;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Payment> findByType(String type) {
        List<Payment> payments = new ArrayList<Payment>();
        try {
            PreparedStatement preparedStatement= this.connection.
                    prepareStatement("Select * From Payments where paymentType=?");

            preparedStatement.setString(1, type);
            ResultSet resultSet= preparedStatement.executeQuery();
            while(resultSet.next()) {
                Payment payment = new Payment(
                        resultSet.getInt("id"),
                        resultSet.getDate("paymentDate"),
                        resultSet.getString("paymentType"),
                        resultSet.getInt("amount"),
                        resultSet.getInt("custId")
                );
                payments.add(payment);
            }
            return  payments;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int save(Payment payment) {
        return 0;
    }
}
