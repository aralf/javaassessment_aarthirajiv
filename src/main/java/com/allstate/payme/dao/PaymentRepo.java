package com.allstate.payme.dao;

import com.allstate.payme.entities.Payment;
import com.allstate.payme.exceptions.InvalidPaymentException;

import java.util.List;

public interface PaymentRepo {
    public int rowCount();
    public Payment findById(int id);
    public List<Payment> findByType(String type);
    public int save(Payment payment); //throws InvalidPaymentException;

    public boolean isConnected();
}
