package com.allstate.payme.services;

import com.allstate.payme.dao.PaymentRepo;
import com.allstate.payme.entities.Payment;
import com.allstate.payme.exceptions.InvalidPaymentException;
import com.mongodb.MongoException;

import java.util.List;

public class PaymentServiceImpl implements PaymentService {

    private PaymentRepo paymentRepo;

    public PaymentServiceImpl(PaymentRepo paymentRepo) {
        this.paymentRepo = paymentRepo;
    }

    @Override
    public int rowCount() {
        return paymentRepo.rowCount();
    }

    @Override
    public Payment findById(int id) {
        return paymentRepo.findById(id);
    }

    @Override
    public List<Payment> findByType(String type) {
        return paymentRepo.findByType(type);
    }

    @Override
    public int save(Payment payment) throws InvalidPaymentException {
        int savedId = -1;
        try {
            savedId = paymentRepo.save(payment);
        } catch(MongoException e) {
            throw new InvalidPaymentException(e.getMessage());
        }
        return savedId;
    }
}
