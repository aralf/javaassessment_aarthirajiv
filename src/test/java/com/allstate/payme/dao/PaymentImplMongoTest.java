package com.allstate.payme.dao;

import com.allstate.payme.dao.PaymentImplMongo;
import com.allstate.payme.entities.Payment;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import java.util.*;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.junit.jupiter.api.Assertions.*;

public class PaymentImplMongoTest {
    private PaymentRepo paymentImplMongo;

    @BeforeEach
    public void setup() {
        paymentImplMongo = new PaymentImplMongo();
    }

    @Test
    public void z_rowCountTest() {
        //get row count
        assertEquals(8, paymentImplMongo.rowCount());
    }

    @Test
    public void findByIdTest() {
        //find by Id
        Payment payment = paymentImplMongo.findById(109);
        assertNotNull(payment);
    }

    @Test
    public void findByTypeTest() {
        //get by type
        List<Payment> paymentList =  paymentImplMongo.findByType("Hourly");
        for(Payment p : paymentList) {
            System.out.println("Inside findByTypeTest "+p.getId()+" ***** "+p.getType());
        }
        assertTrue(paymentList.size() >= 1);
    }

    @Test
    public void saveTest() {
        // insert
        Payment payment = new Payment(107, new Date(), "Daily", 22, 20);
        int save = paymentImplMongo.save(payment);
        assertTrue(save != -1);
    }

    /*@Test
    public void saveDuplicateExceptionTest() {
        //assertThrows int save = paymentImplMongo.save(payment);
    }
    */
}