package com.allstate.payme.dao;

import com.allstate.payme.entities.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class PaymentImplMySqlTest {

    private PaymentRepo paymentImplMySql;

    @BeforeEach
    public void setup() {
        paymentImplMySql = new PaymentImplMySql();
    }

    @Test
    public void testIsConnectedReturnsTrue() {
        assertTrue(paymentImplMySql.isConnected());
    }

    @Test
    public void z_rowCountTest() {
        //get row count
        assertEquals(2, paymentImplMySql.rowCount());
    }

    @Test
    public void findByIdTest() {
        //find by Id
        Payment payment = paymentImplMySql.findById(102);
        assertNotNull(payment);
    }

    @Test
    public void findByType() {
        List<Payment> paymentList =  paymentImplMySql.findByType("Hourly");
        for(Payment p : paymentList) {
            System.out.println("Inside findByTypeTest "+p.getId()+" ***** "+p.getType());
        }
        assertTrue(paymentList.size() >= 1);
    }
}
