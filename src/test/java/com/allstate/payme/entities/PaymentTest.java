package com.allstate.payme.entities;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PaymentTest {

    Payment payment;

   @BeforeEach
   void setup() {
       payment = new Payment();
       payment.setType("Hourly");
       payment.setCustId(10);
       payment.setAmount(333);
   }

    @Test
    public void paymentToString() {
        //
        assertEquals("", "");
    }

    public PaymentTest(int id, Date paymentDate, String type, double amount, int custId) {
        this.id = id;
        this.paymentDate = paymentDate;
        this.type = type;
        this.amount = amount;
        this.custId = custId;
    }

    private int id;
    private Date paymentDate;
    private String type;
    private double amount;
    private int custId;
}
