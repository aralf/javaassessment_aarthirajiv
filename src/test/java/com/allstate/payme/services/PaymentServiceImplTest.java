package com.allstate.payme.services;

import com.allstate.payme.dao.PaymentImplMongo;
import com.allstate.payme.dao.PaymentRepo;
import com.allstate.payme.entities.Payment;
import com.allstate.payme.exceptions.InvalidPaymentException;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import java.util.*;

public class PaymentServiceImplTest {
    private PaymentServiceImpl paymentServiceImpl;

    @BeforeEach
    public void setup () {
        PaymentRepo paymentRepo = new PaymentImplMongo();
        paymentServiceImpl = new PaymentServiceImpl(paymentRepo);
    }

    @Test
    public void findByIdTest() {
        //find by Id
        Payment payment = paymentServiceImpl.findById(107);
        assertNotNull(payment);
    }

    @Test
    public void findByTypeTest() {
        //get by type
        List<Payment> paymentList =  paymentServiceImpl.findByType("Hourly");
        for(Payment p : paymentList) {
            System.out.println("Inside findByTypeTest "+p.getId()+" ***** "+p.getType());
        }
        assertTrue(paymentList.size() >= 1);
    }

    @Test
    public void saveTest() throws InvalidPaymentException {
        // insert
        Payment payment = new Payment(110, new Date(), "Daily", 22, 20);
        int save = paymentServiceImpl.save(payment);
        assertTrue(save != -1);
    }

    @Test
    public void saveDuplicateIdExceptionTest() {

        Exception exception = assertThrows(InvalidPaymentException.class, () -> {
            Payment payment = new Payment(102, new Date(), "Daily", 22, 20);
            int save = paymentServiceImpl.save(payment);
        });
    }

    @Test
    public void z_rowCountTest() {
        //get row count
        assertEquals(10, paymentServiceImpl.rowCount());
    }
}
